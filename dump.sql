-- TODO: your schema here!
CREATE TABLE MyGuests(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
reg_date TIMESTAMP
);

INSERT INTO MyGuests (firstname, lastname, email)
VALUES ('John', 'Deere', 'john@deere.com');
INSERT INTO MyGuests (firstname, lastname, email)
VALUES ('Achilles', 'Sincere', 'achilles@sincere.com')
INSERT INTO MyGuests (firstname, lastname, email)
VALUES ('Simo Tõnu', 'Prees', 'simotonu.prees@tptlive.ee')